const gulp = require('gulp');
const angularTemplatecache = require('gulp-angular-templatecache');
const pug = require('gulp-pug');

const conf = require('../conf/gulp.conf');

gulp.task('copyPartials', copyPartials);
gulp.task('pugifyPartials', pugifyPartials);
gulp.task('partials', gulp.series('pugifyPartials', 'copyPartials'));


function copyPartials() {
  return gulp.src(conf.path.tmp('**/*.html'))
    .pipe(angularTemplatecache('templateCacheHtml.js', {
      module: conf.ngModule,
      root: 'app'
    }))
    .pipe(gulp.dest(conf.path.tmp()));
}

function pugifyPartials() {
  return gulp.src([conf.path.src('app/**/*.pug'), conf.path.src('index.pug')])
    .pipe(pug())
    .pipe(gulp.dest(conf.path.tmp()));
}
