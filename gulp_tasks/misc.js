const path = require('path');

const gulp = require('gulp');
const del = require('del');
const filter = require('gulp-filter');

const conf = require('../conf/gulp.conf');

gulp.task('clean', clean);
gulp.task('other', other);
gulp.task('assets', assets);
gulp.task('icons', icons);

function clean() {
  return del([conf.paths.dist, conf.paths.tmp]);
}

function other() {
  const fileFilter = filter(file => file.stat.isFile());

  return gulp.src([
    path.join(conf.paths.src, '/**/*'),
    path.join(`!${conf.paths.src}`, '/**/*.{scss,js,html}')
  ])
    .pipe(fileFilter)
    .pipe(gulp.dest(conf.paths.dist));
}

function assets(){
  return gulp.src([
    path.join(conf.paths.aftoolkit, '/content/**/*'),
    path.join(`!${conf.paths.aftoolkit}`, '/content/img/slider/*'),
    path.join(`!${conf.paths.aftoolkit}`, '/content/img/slider')
  ])
    .pipe(gulp.dest(conf.paths.assets));
}

function icons() {
  return gulp.src([
    path.join(conf.paths.aftoolkit, '/build/content/fonts/**/*')
  ])
    .pipe(gulp.dest(`${conf.paths.assets}/fonts`))
}
