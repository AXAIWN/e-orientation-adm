# eOrientation ADM

eOrientation Administration website

## Stack
- Angular 1.5.x
- Angular UI Router
- ES6 / babel
- Sass
- Browsersync


## Dependency

### AF.Toolkit
[AF.Toolkit - Github Enterprise](https://github.axa.com/af-webcenter/AF.Toolkit.git)

Edit `.gitmodules` file to contain your credentials (OAuth Tolkens)
The `url` parameter should be as follow:
url = https://**username:token@**github.axa.com/af-webcenter/AF.Toolkit.git


## Run
// Authenticated from Github Enterprise
```
  git clone --recursive
  cd e-orientation-adm
  npm i && bower i

  cd AF.TOOLKIT
  npm i && bower i

  grunt clean html2js buildall webfont copy
  cp conf/af-ui-toolkit-package.json AF.TOOLKIT/build/bower.json

```
ou

```
  git clone
  cd e-orientation-adm
  npm i && bower i

  cd AF.TOOLKIT
  git submodule init
  git submodule update
  npm i && bower i
  grunt clean html2js buildall webfont copy
  cd ..
  cp conf/af-ui-toolkit-package.json AF.TOOLKIT/build/bower.json

```
Then navigate...

##
