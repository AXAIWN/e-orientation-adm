angular
  .module('app', [
    'ui.router',
    'af.uiToolkit.navbar',
    'af.uiToolkit.aside',
    'af.uiToolkit.modal',
    'af.uiToolkit.tooltip',
    'af.uiToolkit.tab',
    'af.uiToolkit.collapse',
    'af.uiToolkit.dropdown',
    'af.uiToolkit.affix',
    'af.uiToolkit.table'
  ]);
