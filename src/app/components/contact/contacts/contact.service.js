function ContactService() {
  return {
    createNewContact: function (contact) {
      return true;
    },
    getContactById: function (id) {
      return true;
    },
    getContactList: function () {
      return [{name: 'Gab'}, {name: 'Sam'}];
    },
    updateContact: function (contact) {
      return true;
    },
    deleteContact: function (contact) {
      return true;
    }
  };
}

/**
 * @ngdoc service
 * @name ContactService
 * @module components.contact
 *
 * @description Provides HTTP methods for server connection.
 *
 */

angular
  .module('components.contact')
  .factory('ContactService', ContactService);
