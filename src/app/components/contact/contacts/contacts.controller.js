function ContactsController($state) {
  var ctrl = this;
  var contacts = ctrl.contacts;

  ctrl.filteredContacts = contacts;
}

angular
  .module('components.contact')
  .controller('ContactsController', ContactsController);
